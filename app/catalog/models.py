from django.db import models


class BaseModel(models.Model):
    is_active = models.BooleanField(default=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

"""
#
# CATEGORY
#
"""
class Category(BaseModel):
    id_category = models.IntegerField(null=True)#icecat_id
    name = models.CharField('Name', max_length=254)

    #parent = models.ForeignKey('self', null=True, blank=True, related_name='children')

"""
#
# Brand
#
"""
class Brand(BaseModel):
    id_brand = models.IntegerField(null=True)#icecat_id
    name = models.CharField('Name', max_length=254)

"""
#
# PRODUCT
#
"""
class Product(BaseModel):
    id_product = models.CharField(null=True, max_length=255)#icecat_id
    title = models.CharField('Title', max_length=1000)
    code_product = models.CharField('Code product', max_length=1000)
    name_product = models.CharField('Name product', max_length=1000)
    ean_product = models.CharField('EAN product', max_length=1000)

    short_decription = models.TextField('short_decription')
    long_decription = models.TextField('long_decription')
    decription = models.TextField('decription')

    thumbnail = models.URLField(max_length=1024, default='')
    url = models.CharField('URL', max_length=254)

    category = models.ForeignKey(Category, null=True, related_name="category_product")
    brand = models.ForeignKey(Brand, null=True, related_name="brand_product")

    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

"""
#
# GroupsFeatures(groups haracteristiki)
#
"""
class GroupsFeatures(BaseModel):
    id_groups_features = models.IntegerField(null=True)#icecat_id

    name = models.CharField('name', max_length=1000)

"""
#
# Features(haracteristiki)
#
"""
class Features(BaseModel):
    id_features = models.IntegerField(null=True)#icecat_id

    name = models.CharField('name', max_length=350)
    #value = models.CharField('value', max_length=450)

    group = models.ForeignKey(GroupsFeatures, null=True, related_name='group_features')

"""
#
# FeaturesProduct(haracteristiki product)
#
"""
class FeaturesProduct(BaseModel):
    product = models.ForeignKey(Product, related_name='product')
    features = models.ForeignKey(Features, related_name='features_group_features')
    value = models.CharField('value', max_length=450)

"""
#
# ImageProduct
#
"""
def image_upload_path(instance, filename):
    """Generates upload path for FileField"""
    return u"images/%s/%s" % (instance.product.id, filename)

class ImageProduct(BaseModel):
    product = models.ForeignKey(Product, related_name='product_image')
    file = models.FileField(upload_to=image_upload_path, blank=True)
    url = models.CharField('URL', max_length=1200)

