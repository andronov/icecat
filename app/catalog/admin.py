from django.contrib import admin

# Register your models here.
from catalog.models import *

admin.site.register(Category)
admin.site.register(Brand)
admin.site.register(Product)
admin.site.register(ImageProduct)
admin.site.register(GroupsFeatures)
admin.site.register(Features)
admin.site.register(FeaturesProduct)