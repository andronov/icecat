# -*- coding: utf-8 -*-
import gzip
from optparse import make_option
import os
from urllib import urlopen
from datetime import datetime
import urllib
import urllib2
from urlparse import urlparse
from django.conf import settings
from django.core.files.temp import NamedTemporaryFile
from django.core.files.uploadedfile import SimpleUploadedFile
from django.shortcuts import render
from xml.dom.minidom import *
# Create your views here.
import requests
from catalog.models import Category, Product, GroupsFeatures, Features, FeaturesProduct, ImageProduct, Brand
from django.core.files import File
from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        parser.add_argument('--ean', nargs='+', type=str)
        parser.add_argument('--noimage',
            action='store_true',
            dest='noimage',
            default=True,
            help='No download image')

    def handle(self, *args, **options):
            file  = requests.get('http://data.icecat.biz/xml_s3/xml_server3.cgi?ean_upc='+str(options['ean'][0])+';lang=ru;output=productxml',
                                    auth=(settings.API_USERNAME, settings.API_PASSWORD))
            xml = parseString(file.content)

            featuress = []
            prod_features = []
            images = []
            brand_id = ''
            brand_name = ''
            prod_title = ''
            prod_tumb = ''
            prod_id = ''
            prodID = ''

            short_decription = ''
            long_decription = ''
            long_decription = ''

            EAN_CODE = ''

            Date_Added = ''
            Updated = ''

            category = ''
            category_value = ''

            if xml:


               category = xml.getElementsByTagName("Category")[0].getAttribute("ID")
               category_value = xml.getElementsByTagName("Category")[0].getElementsByTagName("Name")[0].getAttribute("Value")

               prod_title = xml.getElementsByTagName("Product")[0].getAttribute("Title")
               prod_tumb = xml.getElementsByTagName("Product")[0].getAttribute("HighPic")
               prod_id = xml.getElementsByTagName("Product")[0].getAttribute("Prod_id")
               EAN_CODE = xml.getElementsByTagName("EANCode")[0].getAttribute("EAN")

               prodID = xml.getElementsByTagName("Product")[0].getAttribute("ID")

               brand_id = xml.getElementsByTagName("Supplier")[0].getAttribute("ID")
               brand_name = xml.getElementsByTagName("Supplier")[0].getAttribute("Name")


               short_decription = xml.getElementsByTagName("ShortSummaryDescription")[0].firstChild.nodeValue
               long_decription = xml.getElementsByTagName("LongSummaryDescription")[0].firstChild.nodeValue


               feat = xml.getElementsByTagName("CategoryFeatureGroup")
               for f in  feat:
                   featuress.append({f.getAttribute("ID"): f.getElementsByTagName("Name")[0].getAttribute("Value")})

               prod_feat = xml.getElementsByTagName("ProductFeature")
               for pf in prod_feat:
                   pid = pf.getAttribute("ID")
                   pcid = pf.getAttribute("CategoryFeatureGroup_ID")
                   prv = pf.getAttribute("Presentation_Value")
                   pv =pf.getAttribute("Value")
                   pvn = pf.getElementsByTagName("Name")[0].getAttribute("Value")
                   prod_features.append({pcid: {'id': pid, 'pcid': pcid, 'prv': prv, 'pv': pv, 'pvn': pvn}})

               img = xml.getElementsByTagName('ProductPicture')
               for im in img:
                   if im.getAttribute("Original"):
                      images.append(im.getAttribute("Original"))


               category, created = Category.objects.get_or_create(id_category=int(category),
                                                             defaults={'name': category_value,})
               brand, created = Brand.objects.get_or_create(id_brand=int(brand_id),
                                                             defaults={'name': brand_name,})


               product, created = Product.objects.get_or_create(id_product=prodID,
                                                             defaults={
                                                                 'title': prod_title,
                                                                 'code_product': prod_id,
                                                                 'ean_product': EAN_CODE,
                                                                 'short_decription': short_decription,
                                                                 'long_decription' : long_decription,
                                                                 'thumbnail' : prod_tumb,
                                                                 'category': category,
                                                                 'brand': brand,
                                                             })

               if images and options['noimage']:
                    for ie in images:
                        image, created = ImageProduct.objects.get_or_create(url=ie, defaults={'product': product})
                        if created:
                           img_temp = NamedTemporaryFile()
                           img_temp.write(urllib2.urlopen(ie).read())
                           img_temp.flush()
                           name = urlparse(ie).path.split('/')[-1]
                           image.file.save(name,  File(img_temp), save=True)
                           img_temp.close()

               for feat in featuress:
                    for s in feat.items():
                        groups_features, created = GroupsFeatures.objects.get_or_create(id_groups_features=int(s[0]),
                                                             defaults={'name': s[1],})


               for prod_feat in prod_features:
                    for pf in prod_feat.items():
                        obj_features, created = Features.objects.get_or_create(id_features=int(pf[1].get('id')),
                                                             defaults={'name': pf[1].get('pvn'),
                                                                       'group': GroupsFeatures.objects.get(id_groups_features=int(pf[0]))})
                        features_product = FeaturesProduct.objects.get_or_create(product=product,features=obj_features,
                                                             defaults={
                                                                 'value': pf[1].get('prv'),
                                                             })

